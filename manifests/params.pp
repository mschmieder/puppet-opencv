class opencv::params {
   $version = "2.4.11"
   $manage_packages=true
   $dependencies_only=false
   case $::operatingsystem {
       # Install OpenCV from source. This is an instructive example
       # and may come in handy if we need to move to a version of
       # OpenCV newer than is packaged in the OS.
       ubuntu: {
          $dependencies = ['libjpeg-dev',
                        'libtiff5-dev',
                        'libjasper-dev',
                        'libgtk2.0-dev',
                        'libavcodec-dev',
                        'libavformat-dev',
                        'libswscale-dev',
                        'libdc1394-22-dev',
                        'libv4l-dev',
                        'libxine2-dev',
                        'libgstreamer0.10-dev',
                        'libgstreamer-plugins-base0.10-dev',
                        'libtbb-dev',
                        'python-numpy',
                        'qt5-default',
                        'unzip']
          $packages = ['libopencv*'] 
       }
       /(centos|redhat|oel)/: {
         $packages = ['opencv']
         $dependencies = []
       }
      default: {
       fail("unsupported platform ${::operatingsystem}")
      }
   }
}