class opencv ($version=$::opencv::params::version,
              $from_sources=$::opencv::params::from_source,
              $manage_packages=$::opencv::params::manage_packages,
              $dependencies_only=$::opencv::params::dependencies_only,
) inherits ::opencv::params {
    Exec { path => "/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin" }

    class { 'opencv::installer' :    
        packages => $::opencv::params::packages,
        dependencies => $::opencv::params::dependencies,
        from_source => $from_source,
        manage_packages => $manage_packages,
        dependencies_only => $manage_packages,
    }

    if($from_source){
        class{ 'opencv::build' :
            require => Class['opencv::installer'],
            version => $version 
        }        
    }
}