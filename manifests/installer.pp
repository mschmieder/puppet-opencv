class opencv::installer($packages,
                    $dependencies,
                    $from_source,
                    $manage_packages,
                    $dependencies_only) 
{
    if($dependencies_only and $manage_packages){
        package { $dependencies :
            ensure => installed,
        }
    }
    elsif(!$from_source and $manage_packages){
        $_install_packages=join($dependencies,$packages)
        package { $_install_packages :
            ensure => installed,
        }
    }
    elsif($from_source and $manage_packages){
        package { $dependencies :
            ensure => installed,
        }
    }
}