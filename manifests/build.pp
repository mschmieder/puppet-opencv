class opencv::build($version) {
    $libopencv_core_filename = "libopencv_core.so.${version}"

    exec{"download_opencv" :
        command => "wget http://netcologne.dl.sourceforge.net/project/opencvlibrary/opencv-unix/${version}/opencv-${version}.zip",
        cwd     => "/tmp",
        creates => "/tmp/opencv-${version}.zip",
    }
    exec {"extract_opencv" :
        require => Exec["download_opencv"],
        command => "unzip opencv-${version}.zip",
        cwd => '/tmp/',
        creates => "/tmp/opencv-${version}",
    }
    file { "/tmp/opencv-${version}/build" :
        require => Exec["extract_opencv"],
        ensure => "directory",
    }
    exec {"build_opencv_make" :
        require => [File["/tmp/opencv-${version}/build"]],
        command => "cmake -DCMAKE_BUILD_TYPE=RELEASE -DCMAKE_PREFIX_PATH= -DCMAKE_INSTALL_PREFIX=/usr/local -DWITH_TBB=ON -DBUILD_NEW_PYTHON_SUPPORT=ON -DWITH_V4L=ON -DINSTALL_C_EXAMPLES=OFF -DINSTALL_PYTHON_EXAMPLES=OFF -DBUILD_EXAMPLES=OFF -DWITH_QT=ON -DWITH_OPENGL=ON /tmp/opencv-${version}",
        cwd => "/tmp/opencv-${version}/build",
    }
    exec {"build_opencv" :
        require => Exec["build_opencv_make"],
        command => 'make -j; make install',
        cwd => "/tmp/opencv-${version}/build",
        timeout => 1800,
        creates => "/usr/local/lib/${libopencv_core_filename}",
    }
    exec {"load_module" :
        require => Exec["build_opencv"],
        command => "ldconfig",
    }
}